import * as config from "./config.js";
import data from "../data.mjs";

const rooms = [{ roomId: "fake", roomPlayers: [{ username: "alex", status: "notReady" }, { username: "ololo", status: "ready" }] }];
const activeUsers = new Map();

export default io => {
    io.on("connection", socket => {
        const username = socket.handshake.query.username;
        const socket_id = socket.id;
        if (activeUsers.has(username)) {
            socket.emit("LOGIN_FAILED");
        } else {
            activeUsers.set(username, socket_id);
            socket.emit("UPDATE_ROOMS", rooms);
            socket.emit("LOGIN_SUCCESS", username);
        }

        socket.on("CREATE_ROOM", roomName => {
            const roomId = roomName;
            const currRoom = { roomId, roomPlayers: [{ username, status: "notReady" }] };
            rooms.push(currRoom);

            socket.join(roomId, () => {
                socket['joinedRoom'] = roomId;
                io.emit("UPDATE_ROOMS", rooms);
                io.to(socket.id).emit("CREATE_ROOM_DONE", currRoom, username);
            });
        });

        socket.on("GET_ROOMS", (roomLeft = 'a', username) => {
            if (roomLeft.length > 1) {

                socket.leave(roomLeft);

                const leftRoomIndex = rooms.findIndex(x => x.roomId === (Object.keys(socket.rooms)[0] === roomLeft ? Object.keys(socket.rooms)[0] : Object.keys(socket.rooms)[1]));

                const currUserIndex = rooms[leftRoomIndex].roomPlayers.findIndex(x => x.username === username);
                rooms[leftRoomIndex].roomPlayers.splice(currUserIndex, 1);
                const currRoom = rooms[leftRoomIndex];
                io.to(socket.id).emit("UPDATE_ROOMS", rooms);
                io.emit("UPDATE_USERS_STATUSES", currRoom, username);

            } else {
                io.emit("UPDATE_ROOMS", rooms);
            }

        });

        socket.on("JOIN_ROOM", roomId => {
            socket.join(roomId, () => {
                const currIndex = rooms.findIndex(x => x.roomId === roomId);
                rooms[currIndex].roomPlayers.push({ username: username, status: "notReady" });
                const currRoom = rooms[currIndex];
                io.to(socket.id).emit("JOIN_ROOM_DONE", currRoom);
                io.emit("UPDATE_USERS_STATUSES", currRoom, username);
                io.emit("UPDATE_ROOMS", rooms, false);
            });
        });


        socket.on("USER_READY", roomId => {
            const currIndex = rooms.findIndex(x => x.roomId === roomId);
            const currRoom = rooms[currIndex];
            const currUserIndex = rooms[currIndex].roomPlayers.findIndex(x => x.username === username);
            const currUser = rooms[currIndex].roomPlayers[currUserIndex];
            currUser.status === "notReady" ? currUser.status = "ready" : currUser.status = "notReady";
            const readyUsersInRoom = [];
            const allUsersInRoom = rooms[currIndex].roomPlayers.length;
            for (let i = 0; i < allUsersInRoom; i++) {

                for (var key in rooms[currIndex].roomPlayers[i]) {
                    var value = rooms[currIndex].roomPlayers[i][key];
                    if (value === "ready") readyUsersInRoom.push(value);
                }
            }

            if (readyUsersInRoom.length === allUsersInRoom) {
                const randomTextId = Math.floor(Math.random() * data.texts.length) + 1;

                io.emit("USERS_IN_ROOM_READY", randomTextId, config.SECONDS_TIMER_BEFORE_START_GAME);
            }
            io.emit("UPDATE_USERS_STATUSES", currRoom);
        });

        socket.on("TYPING", (charToHighlight, user) => {
            io.to(socket.id).emit("UPDATE_TEXT_COLOR", charToHighlight);
            io.emit("UPDATE_PROGRESS_BAR", charToHighlight, user);

        });

        socket.on("GAME_OVER", winnerName => {
            io.emit("SHOW_WINNER", winnerName)
        })

        socket.on('disconnecting', () => {
            // hardcoded to work with created room, not "fake"
            // need to create separate func for this ?
            const currIndex = rooms.findIndex(x => x.roomId === (Object.keys(socket.rooms)[0] == socket_id ? Object.keys(socket.rooms)[0] : Object.keys(socket.rooms)[1]));

            const currRoom = rooms[currIndex];
            if (currRoom && currIndex) {

                const leftUser = currRoom.roomPlayers.findIndex(x => x.username === username);
                rooms[currIndex].roomPlayers.splice(leftUser, 1);
                if (rooms[currIndex].roomPlayers.length == 0) rooms.splice(currIndex, 1); // if room empty -- delete room from "rooms"
                const updatedRoom = rooms[currIndex];
                io.emit("UPDATE_USERS_STATUSES", updatedRoom, username);
                io.emit("UPDATE_ROOMS", rooms, false);
            }
        });

        socket.on("disconnect", () => {
            activeUsers.forEach((value, key) => {
                if (value === socket_id) {
                    activeUsers.delete(key);
                }
            })
        });

    });
};