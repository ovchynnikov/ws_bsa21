import { texts } from './data.mjs';
// import из этого места иначе ошибка постоянная "Expected a JavaScript module script but the server responded with a MIME type of "text/html" ".. 
const username = sessionStorage.getItem("username");

const usernameGreetings = document.getElementById("username");
const createRoomButton = document.getElementById("add-room-btn");
const roomsContainer = document.getElementById("rooms-container");
const roomsPage = document.getElementById("rooms-page");
const gameInterface = document.getElementById("game-page");
const readyButton = document.getElementById("ready-btn");
const joinButton = document.getElementsByClassName("join-btn")[0];
const gameRoomInfo = document.getElementsByClassName("game-room-info")[0];

if (!username) {
    window.location.replace("/login");
}

const socket = io("", { query: { username } });

const loginFailHandler = () => {
    window.alert("This username is already taken");
    sessionStorage.removeItem("username");
    window.location.replace("/login");
}

const loginSuccessHandler = username => {
    sessionStorage.removeItem("roomName");

    usernameGreetings.innerHTML = `Hello ${username},`;
}

const createRoomHandler = () => {
    const roomName = prompt("Enter room name");
    socket.emit("CREATE_ROOM", roomName);
};

createRoomButton.addEventListener("click", createRoomHandler);

const renderRoomsTrigger = () => {
    const roomLeft = sessionStorage.getItem("roomName");
    const username = sessionStorage.getItem("username");
    sessionStorage.removeItem("roomName");
    socket.emit("GET_ROOMS", roomLeft, username);
};

const renderRooms = (roomInfo, hideGamePage = true) => {

    const userInRoom = sessionStorage.getItem("roomName");
    if (userInRoom) return
    const gameInterface = document.getElementById("game-page");

    if (hideGamePage) roomsPage.className = "full-screen";

    roomsContainer.innerHTML = "";
    if (hideGamePage) gameInterface.className = "full-screen display-none";

    for (let i = 0; i < roomInfo.length; i++) {

        const divRoom = document.createElement("div");
        divRoom.className = "room";

        const spanUsers = document.createElement("span");
        spanUsers.textContent = `${roomInfo[i].roomPlayers.length} user connected`;

        const roomName = document.createElement("h2");
        roomName.textContent = `Room №${roomInfo[i].roomId}`;

        const joinButton = document.createElement("button");
        joinButton.textContent = "Join Room";
        joinButton.className = `join-btn btn ${roomInfo[i]['roomId']}`;
        joinButton.value = `${roomInfo[i]['roomId']}`;

        divRoom.appendChild(spanUsers);
        divRoom.appendChild(roomName);
        divRoom.appendChild(joinButton);
        roomsContainer.appendChild(divRoom);

        const renderedJoinButton = document.getElementsByClassName(`${roomInfo[i]['roomId']}`)[0];
        renderedJoinButton.addEventListener("click", joinRoomHandler);
    }

}

const joinRoomHandler = e => {
    const roomId = e.target.value;
    socket.emit("JOIN_ROOM", roomId);
}

const updateUsersInfo = (roomInfo) => {
    const username = sessionStorage.getItem("username");

    const gameRoomInfo = document.getElementsByClassName("game-room-info")[0];
    const oldUsersContainer = document.getElementsByClassName("users-container")[0];
    gameRoomInfo.removeChild(oldUsersContainer);
    const { roomPlayers } = roomInfo;

    const usersContainer = document.createElement("div");
    usersContainer.setAttribute("class", "users-container");
    usersContainer.setAttribute("id", "users-container");

    for (let i = 0; i < roomPlayers.length; i++) {
        const roomPlayer = roomInfo.roomPlayers[i];
        const divUser = document.createElement("div");

        divUser.className = "user";

        const spanDot = document.createElement("span");
        spanDot.setAttribute("class", `dot ${roomPlayer['username']}`);
        spanDot.style.backgroundColor = roomPlayer['status'] === "notReady" ? `red` : `green`;

        const spanUserName = document.createElement("span");
        spanUserName.textContent = roomPlayer['username'] === username ? `${roomPlayer['username']} (you)` : `${roomPlayer['username']}`;
        spanUserName.setAttribute("id", "user-name-span");

        const divBar = document.createElement("div");
        divBar.className = "loading-bar";

        const divProgress = document.createElement("div");
        divProgress.className = `loading-bar-progress loading-bar-progress-${roomPlayer['username']}`;

        divBar.appendChild(divProgress);

        divUser.appendChild(spanDot);
        divUser.appendChild(spanUserName);
        divUser.appendChild(divBar);

        gameRoomInfo.appendChild(usersContainer);

        usersContainer.appendChild(divUser);
    }

}

const renderJoinedRoom = (roomInfo) => {
    sessionStorage.setItem("roomName", roomInfo.roomId);

    roomsPage.setAttribute("class", "full-screen display-none");
    gameInterface.setAttribute("class", "full-screen");
    const roomName = document.createElement("h2");
    roomName.textContent = `Room № ${roomInfo.roomId}`;

    const quitButton = document.createElement("button");
    quitButton.textContent = "Back To Rooms";
    quitButton.setAttribute("id", "quit-room-btn");

    gameRoomInfo.appendChild(roomName);
    gameRoomInfo.appendChild(quitButton);

    updateUsersInfo(roomInfo);

    const backToRoomsButton = document.getElementById("quit-room-btn");

    backToRoomsButton.addEventListener("click", renderRoomsTrigger);
}


const userReadyToggler = () => {
    const roomId = sessionStorage.getItem("roomName");
    socket.emit("USER_READY", roomId);
    readyButton.textContent === "READY" ? readyButton.textContent = "NOT READY" : readyButton.textContent = "READY";
}

readyButton.addEventListener("click", userReadyToggler);

let gameText = "";

const startTheGame = (gameTextid, timer) => {
    readyButton.setAttribute("class", "display-none");
    const backToRoomsButton = document.getElementById("quit-room-btn");
    backToRoomsButton.setAttribute("class", "display-none");
    const gameTextContainer = document.getElementsByClassName("game-text-container")[0];

    const timerContainer = document.createElement("h2");
    timerContainer.setAttribute("id", "timer");
    gameTextContainer.appendChild(timerContainer);
    const timerOnScreen = document.getElementById("timer");

    let seconds = timer;
    const x = setInterval(function() {
        if (seconds === 0) {
            clearInterval(x);
            timerOnScreen.innerHTML = `${seconds}`;
            document.addEventListener("keypress", handleKeyPress);

            setTimeout(async() => {
                gameTextContainer.removeChild(timerOnScreen);

                const textData = await fakeCallApi(`/game/texts/:${gameTextid}`)
                    .then(response => document.getElementById("normal-text").innerHTML = response)
                    .catch(err => console.log(err));
                gameText = textData;

            }, 1000);
        } else {
            timerOnScreen.innerHTML = `${seconds}`;
            seconds--;
        }
    }, 1000);

    const getTextById = textId => {
        return texts[textId];
    }

    async function fakeCallApi(endpoint) {
        const response = endpoint === `/game/texts/:${gameTextid}` ? await getTextById(gameTextid) : null;

        return new Promise((resolve, reject) => {
            setTimeout(() => (response ? resolve(response) : reject(Error('Failed to load'))), 500);
        });
    }

}

const getPercent = (currentPosition, textLength) => {
    return (currentPosition / textLength) * 100;
}

let nextCharrIndex = 0;
let charsToHighlight = 0;

const handleKeyPress = (e) => {
    const gameTextLength = gameText.length;

    const user = sessionStorage.getItem("username");

    if (gameTextLength >= nextCharrIndex && e.key === gameText[nextCharrIndex]) {
        nextCharrIndex++;
        charsToHighlight++;
        socket.emit("TYPING", charsToHighlight, user);
    }

}

const updateProgressBar = (currentPosition, user) => {
    const gameTextLength = gameText.length;
    let progressPercentage = getPercent(currentPosition, gameTextLength);
    const progressBar = document.getElementsByClassName(`loading-bar-progress-${user}`)[0];
    progressBar.style.width = `${progressPercentage}%`
}

const highlightGameText = charToHighlight => {

    let colorText = gameText.slice(0, charToHighlight);
    let updatedGameText = gameText.slice(charToHighlight);
    document.getElementById("mark").innerHTML = `${colorText}`;
    document.getElementById("normal-text").innerHTML = `${updatedGameText}`;
    if (charToHighlight === gameText.length) gameOver();
}

const gameOver = () => {
    const user = sessionStorage.getItem("username");

    socket.emit("GAME_OVER", user);
}

const showTheWinnerName = user => {
    alert(`${user} is a winner`);
}

socket.on("LOGIN_FAILED", loginFailHandler);
socket.on("LOGIN_SUCCESS", loginSuccessHandler);

socket.on("CREATE_ROOM_DONE", renderJoinedRoom);
socket.on("UPDATE_ROOMS", renderRooms);
socket.on("JOIN_ROOM_DONE", renderJoinedRoom);

socket.on("UPDATE_USERS_STATUSES", updateUsersInfo);
socket.on("USERS_IN_ROOM_READY", startTheGame);

socket.on("UPDATE_PROGRESS_BAR", updateProgressBar);
socket.on("UPDATE_TEXT_COLOR", highlightGameText);

socket.on("SHOW_WINNER", showTheWinnerName);